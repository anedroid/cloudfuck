const cfRange = [
    '173.245.48.0/20',
    '103.21.244.0/22',
    '103.22.200.0/22',
    '103.31.4.0/22',
    '141.101.64.0/18',
    '108.162.192.0/18',
    '190.93.240.0/20',
    '188.114.96.0/20',
    '197.234.240.0/22',
    '198.41.128.0/17',
    '162.158.0.0/15',
    '104.16.0.0/13',
    '104.24.0.0/14',
    '172.64.0.0/13',
    '131.0.72.0/22'
];
const cfRange6 = [
    '2400:cb00::/32',
    '2606:4700::/32',
    '2803:f800::/32',
    '2405:b500::/32',
    '2405:8100::/32',
    '2a06:98c0::/29',
    '2c0f:f248::/32'
];
// TODO: implement IPv6 check
function ip42int(address) {
    let _4 = address.split('.');
    let rtn = 0;
    rtn += Number(_4[0]) * 0x100**3;
    rtn += Number(_4[1]) * 0x100**2;
    rtn += Number(_4[2]) * 0x100;
    rtn += Number(_4[3]);
    return rtn;
}
function calc4range(range) {
    let base = range.split('/')[0];
    let prefix = Number(range.split('/')[1]);
    let hostN = 2 ** (32-prefix);
    let hostMin = ip42int(base);
    hostMin -= hostMin % hostN--;
    let hostMax = hostMin + hostN;
    return [hostMin, hostMax];
}
function in_4range(range, address) {
    range = calc4range(range);
    address = ip42int(address);
    return address >= range[0] && address <= range[1];
}
function is_cloudfucked(address) {
    return cfRange.some(range => in_4range(range, address));
}
browser.runtime.onConnect.addListener(function(port) {
    port.onMessage.addListener(async function(data) {
        switch (data.action) {
        case 'test':
            console.log(`testing ${data.hostname}`);
            let result = await browser.dns.resolve(data.hostname, ['disable_ipv6']);
            let cloudfucked = result.addresses.some(ip => is_cloudfucked(ip));
            port.postMessage({
                "hostname": data.hostname,
                "cloudfucked": cloudfucked
            });
            break;
        case 'prefs-update':
            console.log('attempting to update preferences', data.prefs);
            break;
        case 'prefs-fetch':
            console.log('attempting to fetch preferences');
            break;
        default:
            console.log(`unknown action ${data.action}`);
            break;
        }
    });
});
